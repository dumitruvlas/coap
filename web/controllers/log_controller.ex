defmodule Aadya.LogController do
  use Aadya.Web, :controller

  alias Aadya.Log

  def index(conn, _params) do
   
    case File.read("debug.log")  do
      {:error,_} -> log_file = ["error in log file"]
      {:ok,_} ->    log_file = File.stream!("debug.log") |> Enum.to_list |> Enum.reverse
    end

    render(conn, "index.html", logs: log_file)
  end

end
