defmodule Aadya.ResourcesController do
  use Aadya.Web, :controller

  alias Aadya.Resources

  def index(conn, _params) do
    put_res = Application.get_env(:server,:application).put_resources
    delete_res = Application.get_env(:server,:application).delete_resources
    post_res = Application.get_env(:server,:application).post_resources
    get_res = Application.get_env(:server,:application).get_resources
    unobs_res = Application.get_env(:server,:application).unobserve_resources
    render(conn, "index.html", get: put_res, del: delete_res, post: post_res, put: put_res, unob: unobs_res)
  end

end
