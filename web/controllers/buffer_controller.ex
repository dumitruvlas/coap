defmodule Aadya.BufferController do
  use Aadya.Web, :controller

  alias Aadya.Buffer

  def index(conn, _params) do
    render(conn, "index.html")
  end

end
