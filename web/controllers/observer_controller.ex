defmodule Aadya.ObserverController do
  require IEx
  use Aadya.Web, :controller

  alias Aadya.Observer

  def indexapi(conn, _params) do
    case :ets.tab2list :resource_observer do
      [] -> data = "nil" 
      resource_list  ->
       data = resource_list_to_map(resource_list) 
    end
    conn |> json %{status: "true",resources: data}
  end

  def index(conn, _params) do
    case :ets.tab2list :resource_observer do
      [] -> obs_pids = ["no observer yet"] 
      resource_list  ->
       obs_pids = resource_list_to_map(resource_list) 
    end
      render(conn, "index.html", pid: obs_pids )
  end
  def resource_list_to_map(list) do
    for {key,values} <- list, do: {key,values}, into: %{}  
  end

  def delete(conn, params) do
    IO.inspect(params["pid"])
    # Aadya.Message.observer_delete pid
    conn
    |> redirect(to: "/observer")
    |> halt()
  end
end
