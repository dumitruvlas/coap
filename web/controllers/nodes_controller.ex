defmodule Aadya.NodesController do
  use Aadya.Web, :controller

  alias Aadya.Nodes

  def index(conn, _params) do

    case Node.list do
      [] -> node_list = ["no nodes yet"]
      _  -> node_list = Node.list
    end
 
    render(conn, "index.html", nodes: node_list)
  end

end
