defmodule Aadya.Web.Router do
  use Aadya.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Aadya do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/observer", ObserverController, :index
    get "/observer/delete", ObserverController, :delete
    get "/resource", ResourcesController, :index
    get "/log", LogController, :index
    get "/nodes", NodesController, :index
    get "/buffer", BufferController, :index
  end

  scope "/api", Aadya do
    pipe_through :api
    
    get "/observer", ObserverController, :indexapi
    get "/observer/delete", ObserverController, :delete
    get "/resource", ResourcesController, :index
    get "/log", LogController, :index
    get "/nodes", NodesController, :index
    get "/buffer", BufferController, :index
  end

end
