# Defining the records
defmodule Aadya.UdpSocket do
  use GenServer
  require Logger
  require Record
  @timeout 60000
  Record.defrecord :state,[sock: :nil,chans: :nil,pool: :nil]

  @moduledoc ~S"""

  CoapUdpSocket documentation
  dispatcher for UDP communicatins
  Maintains a lookup-table for existing channels
  When a channel pool is provided (server mode), create new channels

  """

  # client
  def start_link do
    GenServer.start_link __MODULE__,[0],[name: __MODULE__]
  end

  #server

  def start_link serverName,inPort do
    GenServer.start_link __MODULE__,[inPort],[]
  end

  @doc ~S"""

  Initiating the UDPScoket
  socket is added to the state

  """

  def init([inPort]) do
    Logger.info "Coap Server Started at port #{@default_coap_port}"
    {:ok, socket} = :gen_udp.open(inPort, [:binary, {:active, true}, {:reuseaddr, true}])
  end

  @doc ~s"""

  New Packet is received from UDP server
  Send data to coap transport pool 

  """

  def handle_info({:udp, socket,peerIP, peerPortNo, data},state) do
    GenServer.cast(:poolboy.checkout(:message),{:in, {state,peerIP,peerPortNo ,data}})
    {:noreply, state}
  end

  @doc ~s"""
  #Respond to packet -- Trediationally a reply or initiating a state/broadcast
  """
  def handle_info({:datagram, {peerIP, peerPortNo}, data}, state(sock: socket)=state) do
    :ok = :gen_udp.send(socket, peerIP, peerPortNo, data)
    {:noreply, state}
  end

  @doc """
  Termination of socket
  Removing the process state
  """
  def handle_info({:terminated, supPid, chId}, state(chans: chans)=state) do
    chans2 = :dict.erase(chId,chans)
    :erlang.exit(supPid,:normal)
    {:noreply, state(state,chans: chans2)}
  end

  def code_change(_OldVsn, state, _Extra) do
    {:ok, state}
  end

end
