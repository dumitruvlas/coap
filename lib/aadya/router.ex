defmodule Aadya.Router do
  defmacro __using__ _options do
    quote do
     import unquote(__MODULE__)
      Module.register_attribute __MODULE__, :get_resources, accumulate: true
      Module.register_attribute __MODULE__, :put_resources, accumulate: true
      Module.register_attribute __MODULE__, :post_resources, accumulate: true
      Module.register_attribute __MODULE__, :delete_resources, accumulate: true
      Module.register_attribute __MODULE__, :unobserve_resources, accumulate: true
      @before_compile unquote(__MODULE__)
    end
  end
 
  defmacro __before_compile__(_options) do
    quote do
      def resource_get(path) do
         {{:error,:not_found}, :nil}
      end
      def resource_put(path,payload) do
         {{:error,:not_found}, :nil}
      end
      def resource_delete(path) do
         {{:error,:not_found}, :nil}
      end
      def resource_post(path,payload) do
         {{:error,:not_found}, :nil}
      end
      def send_message(observers_list,data) do
        observers_list
        |> Enum.map(fn(observer_pid)-> send observer_pid,{:send,data} end )
      end

      def get_resources,do: @get_resources
      def post_resources,do: @post_resources
      def put_resources,do: @put_resources
      def delete_resources,do: @delete_resources
      def unobserve_resources,do: @unobserve_resources
    end
  end

  defmacro get(path, module , function) do
    quote do
     @get_resources unquote(path)
      def resource_get(unquote(path)) do
        apply(unquote(module),unquote(function),[unquote(path)])
      end
    end
  end
  
  defmacro put(path, module , function) do
    quote do
     @put_resources unquote(path)
      def resource_put(unquote(path), payload) do
        case apply(unquote(module),unquote(function),[unquote(path), payload]) do
          {:ok, data} ->
            case :ets.lookup(:resource_observer, unquote(path)) do
              [] ->
                {{:ok,:content}, data}
              [{_,observers_list}] ->
                spawn(__MODULE__,:send_message,[observers_list, data])
                {{:ok,:content}, data}
              _ ->
                {{:ok,:content}, data}
            end
          {:error} ->
            {:error}
        end
      end
    end
  end

  defmacro delete(path, module , function) do
    quote do
     @delete_resources unquote(path)
      def resource_delete(unquote(path), payload) do
        apply(unquote(module),unquote(function),[unquote(path),payload])
      end
    end
  end

  defmacro post(path, module , function) do
    quote do
     @post_resources unquote(path)
      def resource_post(unquote(path), payload) do
        apply(unquote(module),unquote(function),[unquote(path),payload])
      end
    end
  end

  defmacro unobserve(path) do
    quote do
      @unobserve_resources unquote(path)
    end
  end

end
