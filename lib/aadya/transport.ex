defmodule Aadya.Transport do
  use GenServer

  def start_link(_) do
    {:ok,pid}=GenServer.start_link(__MODULE__, nil, [])
  end

  def init(_) do
    {:ok, nil}
  end

  def handle_cast({:in, {socket,peerIP,peerPortNo,data}},state) do
    :poolboy.checkout(:message)
    GenServer.cast(:poolboy.checkout(:transport),{:in, {state,peerIP,peerPortNo ,data}})
    IO.inspect self()
    IO.inspect :poolboy.checkin(:transport,self())
    {:noreply,state}
  end

  def handle_info({:out, { socket,peerIP, peerPortnNo, data}}, state) do
    :gen_udp.send(socket,peerIP, peerPortnNo, data)
    {:noreply,state}
  end
end
