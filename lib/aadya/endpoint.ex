defmodule Aadya.Endpoint do
  use Phoenix.Endpoint, otp_app: :aadya

  plug Plug.Static,
    at: "/", from: :aadya, gzip: false,
    only: ~w(plugins assets css fonts images js favicon.ico robots.txt app.css app.js)

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Poison

  plug Plug.MethodOverride
  plug Plug.Head

  plug Plug.Session,
    store: :cookie,
    key: "_Coap_key",
    signing_salt: "9k9wuAQ7"

  plug Aadya.Web.Router
end
