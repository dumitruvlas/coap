defmodule Aadya.Observer do 
  use GenServer
  require Record
  use Aadya.Header

  require Logger
  def start_link(sock,peer,port, msg_id,token, options) do
    GenServer.start_link(__MODULE__,[sock,peer,port,msg_id,token,options],[])
  end 

  def init([sock,peer,port, msg_id, token, options]) do
    {:ok, {sock,peer,port, msg_id, token, options}}
  end

  def handle_info({:send, payload}, {sock,peer,port, msg_id, token, options} ) do
    options_new = opt_obs_inc options
    Logger.info "observer #{inspect peer} #{port} #{inspect options_new}"
    :ok = :gen_udp.send(sock, peer,port, build_payload(payload,msg_id,token,options_new) )
    ref = Process.send_after(self, :counter_start, 5*1000)
    :ets.insert :resource_observer_msg_id,{ msg_id, [self(),ref]}
    {:noreply, {sock,peer,port, (msg_id+1), token,options_new} }
  end

  def build_payload(payload, msg_id, token, options_new ) do
    msg = coap_message()
    coap_message(
        msg,
        type: :con,
        id: msg_id,
        method: {:ok, :content},
        payload: payload,
        token: token,
        options: options_new 
      )
    |> Aadya.MessageParser.encode
  end

  def opt_obs_inc(options) do
    Keyword.update(options, :observe, :nil, &(&1 + 1))
  end

  def handle_info(:counter_start, state) do
    Logger.info "Observer stop #{inspect state}"
    elem(state,3)
    |> kill_process
    {:noreply, state}
  end

  def handle_info({:counter_clear,ref}, state) do
    Process.cancel_timer ref
    {:noreply, state }
  end

  def kill_process(msg_id) do
    :ets.delete( :resource_observer_msg_id, (msg_id-1))
    [{_,pids}] = :ets.lookup( :resource_observer,"/")
    Logger.info "termintating #{msg_id} #{inspect self()}"
    [{pid,{point,_,_,_,_,_}}] = :ets.lookup(:resource_observer_pid,self())
    [{_,pids_list}] = :ets.lookup(:resource_observer,point)
    :ets.update_element(:resource_observer,point,List.delete(pids_list,self()))
    Process.exit( self(), :kill)
    end
end
