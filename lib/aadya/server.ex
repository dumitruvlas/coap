defmodule Aadya.Server do
  require Record
  use Supervisor
  require Logger
  @default_coap_port 5683

  defp message_config do
    [{:name, {:local, :message }},
     {:worker_module, Aadya.Message},
     {:size, 33 },
     {:max_overflow, 2}
   ]
  end

  def start_link(otp_name) do
    Supervisor.start_link(__MODULE__, [@default_coap_port,otp_name])
  end

  def init([inPort,otp_name]) do 
    import Supervisor.Spec

    children = [
      supervisor(Aadya.Endpoint, []),
      :poolboy.child_spec(:worker, message_config, otp_name),
      supervisor(Aadya.ObserverSup,[{:local, __MODULE__}]),
      worker(Aadya.UdpSocket,[{:local, __MODULE__},inPort])
    ]

    :ets.new(:resource_observer,[:set,:public,:named_table])
    :ets.new(:resource_observer_pid,[:set,:public,:named_table])
    :ets.new(:resource_observer_msg_id,[:set,:public,:named_table])
    :ets.new(:resource,[:set,:public,:named_table])
    :ets.new(:buffer,[:set,:public,:named_table])
    :ets.new(:unobserve_resources,[:set,:public,:named_table])

    opts = [strategy: :one_for_all, name: Aadya.Server.Supervisor]
    supervise(children, opts)

  end

end
