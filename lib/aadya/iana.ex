defmodule Aadya.Iana do
  @moduledoc ~S"""
  CoreIana documentation
  values assingned by IANA
  """
  def content_formats() do
    [{0, <<"text/plain">>},
     {40, <<"application/link-format">>},
     {41, <<"application/xml">>},
     {42, <<"application/octet-stream">>},
     {47, <<"application/exi">>},
     {50, <<"application/json">>},
     {60, <<"application/cbor">>}
   ]
  end

  def decode_enum(dict, value) do
    decode_enum(dict, value, :nil)
  end

  def decode_enum(dict, value, default) do
    case(for {x, y} <- dict, x == value, do: y) do
      [res] -> res
      [] -> default
    end
  end

  def encode_enum(dict, value) do
    encode_enum(dict, value, :nil)
  end

  def encode_enum(dict, value, default) do
    case( for {x, y} <- dict, y == value, do: x) do
      [res] -> res
      [] -> default
    end
  end
end
