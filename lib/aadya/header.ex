defmodule Aadya.Header do
  @moduledoc ~S"""
  CoapChannelH documentation
  documentations is here
  """
  require Record
  defmacro __using__(_) do
    quote do
      @max_block_size 1024
      @default_max_age 60

      @typedoc """
      just coap_message record type
      """
      @type coap_message :: coap_message()

      @typedoc """
      just coap_message record type
      """
      @type coap_content:: coap_content()
      Record.defrecord :coap_message, [type: :nil, method: :nil, id: :nil, token: <<>>, options: [], payload: <<>>]
      Record.defrecord :coap_content, [etag: :nil, max_age: @default_max_age, format: :nil, payload: <<>>]
    end
  end
end
