defmodule Ahamtech do
  use Application
  
  def start() do
    import Supervisor.Spec
    children = [
      supervisor(Aadya.Server,[AhamtechRouter],[])
    ]
    opts = [strategy: :one_for_all, name: Ahamtech.Supervisor]
    Supervisor.start_link(children, opts)
    end
end
