defmodule Hello do

  def hello path do
    case :ets.lookup :resource,path do
      [] ->
        {{:error, :not_found}, :nil }
      [{_,data}] ->
        {{:ok, :content},data}
      end
  end

  def put path,data do
    case :ets.insert(:resource,{path,data}) do
      true -> 
        {:ok, :content}
      _ ->
        IO.puts "something went worong"
        {:error, :not_found}
    end
  end
  def post path,data do
    case :ets.insert(:resource,{path,data}) do
      true -> 
        {{:ok, :content},nil}
      _ ->
        {{:error, :not_found}}
    end
  end
end
