defmodule AhamtechRouter do
  use Aadya.Router

  get "/", Hello, :hello
  post "/", Hello, :post
  put "/", Hello, :put

  get "/help", Hello, :hello
  put "/help", Hello, :put

  get "/helper", Hello, :hello
  put "/helper", Hello, :put

  delete "/help", Hello,:put
  delete "/helper", Hello,:put

  unobserve "/hello"
  unobserve "/helper"
end
