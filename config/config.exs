use Mix.Config

# config :logger,
  # backends: [:console]
#
# config :logger,
  # backends: [{LoggerFileBackend, :debug_log}]

# config :logger, :debug_log,
  # path: "debug.log",
  # level: :debug

# Configures the endpoint
config :aadya, Aadya.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "6wUbtNztAqYI/Bn1o2UZ5+BqC4q4PzptFiO3jYMrFOc47wY21hCk4gfdhuqG6Rwd",
  render_errors: [view: Aadya.ErrorView, accepts: ~w(html json)],
  http: [port: 4000],
  debug_errors: true

  # Configures Elixir's Logger
# config :logger, :console,
  # format: "$time $metadata[$level] $message\n",
  # metadata: [:request_id]
 
config :logger, :logger_papertrail_backend,
host: "logs2.papertrailapp.com:26886",
level: :debug,
system_name: "Aadya",
format: "$metadata $message"

config :logger,
  backends: [ :console,
    LoggerPapertrailBackend.Logger
  ],
  level: :debug
