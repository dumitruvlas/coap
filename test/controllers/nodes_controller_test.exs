defmodule Coap.NodesControllerTest do
  use Coap.ConnCase

  alias Coap.Nodes
  @valid_attrs %{}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, nodes_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing nodes"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, nodes_path(conn, :new)
    assert html_response(conn, 200) =~ "New nodes"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, nodes_path(conn, :create), nodes: @valid_attrs
    assert redirected_to(conn) == nodes_path(conn, :index)
    assert Repo.get_by(Nodes, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, nodes_path(conn, :create), nodes: @invalid_attrs
    assert html_response(conn, 200) =~ "New nodes"
  end

  test "shows chosen resource", %{conn: conn} do
    nodes = Repo.insert! %Nodes{}
    conn = get conn, nodes_path(conn, :show, nodes)
    assert html_response(conn, 200) =~ "Show nodes"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, nodes_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    nodes = Repo.insert! %Nodes{}
    conn = get conn, nodes_path(conn, :edit, nodes)
    assert html_response(conn, 200) =~ "Edit nodes"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    nodes = Repo.insert! %Nodes{}
    conn = put conn, nodes_path(conn, :update, nodes), nodes: @valid_attrs
    assert redirected_to(conn) == nodes_path(conn, :show, nodes)
    assert Repo.get_by(Nodes, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    nodes = Repo.insert! %Nodes{}
    conn = put conn, nodes_path(conn, :update, nodes), nodes: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit nodes"
  end

  test "deletes chosen resource", %{conn: conn} do
    nodes = Repo.insert! %Nodes{}
    conn = delete conn, nodes_path(conn, :delete, nodes)
    assert redirected_to(conn) == nodes_path(conn, :index)
    refute Repo.get(Nodes, nodes.id)
  end
end
